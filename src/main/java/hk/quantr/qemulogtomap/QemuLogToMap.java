package hk.quantr.qemulogtomap;

import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.helper.VariableWithAddress;
import hk.quantr.javalib.PropertyUtil;
import java.io.File;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QemuLogToMap {

	private static final Logger logger = Logger.getLogger(QemuLogToMap.class.getName());
	public static DecimalFormat decimalFormatter = new DecimalFormat("#,###");

	public static void main(String[] args) throws ParseException {
		try {
			Options options = new Options();
			options.addOption("v", "version", false, "display version");
			options.addOption("h", "help");
			options.addOption("g", "gui");
			options.addOption(Option.builder("i")
					.required(false)
					.hasArg()
					.argName("file")
					.desc("input elf file")
					.build());
			options.addOption(Option.builder("d")
					.required(false)
					.hasArg()
					.argName("db")
					.desc("h2 database file")
					.build());
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("java -jar qemu-log-panel.jar [OPTION]", options);
				return;
			}
			if (cmd.hasOption("v")) {
				System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

				TimeZone utc = TimeZone.getTimeZone("UTC");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				sdf.setTimeZone(utc);
				Calendar cl = Calendar.getInstance();
				try {
					Date convertedDate = sdf.parse(PropertyUtil.getProperty("main.properties", "build.date"));
					cl.setTime(convertedDate);
					cl.add(Calendar.HOUR, 8);
				} catch (java.text.ParseException ex) {
					logger.log(Level.SEVERE, null, ex);
				}
				System.out.println("build date : " + sdf.format(cl.getTime()) + " HKT");
				return;
			}

			boolean error = false;
			if (!cmd.hasOption("i")) {
				System.out.println("please specific -i for input elf file");
				error = true;
			}
			if (!cmd.hasOption("d")) {
				System.out.println("please specific -d for h2 database file");
				error = true;
			}
			if (error) {
				return;
			}

			File elf = new File(cmd.getOptionValue("i").replaceFirst("^~", System.getProperty("user.home")));
			ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(elf, 0, false);

//			long address = 0x80005e8al;
//			QuantrDwarf.isDebug = true;
//			Dwarf.showDebugMessage = true;
//			VariableWithAddress v2 = QuantrDwarf.getFilePathAndLineNo(dwarfArrayList, BigInteger.valueOf(address));
//			System.out.println(v2);
//			if (1 < 2) {
//				return;
//			}
			File database = new File(cmd.getOptionValue("d").replaceFirst("^~", System.getProperty("user.home")));
			Connection qemuConn = DriverManager.getConnection("jdbc:h2:" + database.getAbsolutePath().replace(".mv.db", ""), "sa", "");
			Statement statement = qemuConn.createStatement();
			ResultSet rs;
			try {
				String sql = "select * from `qemu` order by sequence limit 0,1000000;";
				System.out.println(sql);
				rs = statement.executeQuery(sql);
				int x = 0;
				String lastFile = "";
				while (rs.next()) {
					long pc = rs.getLong("pc");
					VariableWithAddress v = QuantrDwarf.getFilePathAndLineNo(dwarfArrayList, BigInteger.valueOf(pc));
					if (v == null) {
						System.out.println(decimalFormatter.format(x) + " > " + Long.toHexString(pc));
					} else {
						if (!v.file.getAbsolutePath().equals(lastFile)) {
							lastFile = v.file.getAbsolutePath();
							System.out.println(decimalFormatter.format(x) + " > " + Long.toHexString(pc) + " = " + v.file.getAbsolutePath() + ":" + v.line_num);
						}
					}
					x++;
				}
			} catch (Exception ex) {
				System.out.println("qemu  : 0");
			}
		} catch (SQLException ex) {
			Logger.getLogger(QemuLogToMap.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
